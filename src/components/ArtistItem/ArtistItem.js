import React from 'react';
import {NavLink} from "react-router-dom";
import {apiURL} from '../../constants';
import imageNotAvailable from "../../assets/images/download.png";
import {useDispatch, useSelector} from "react-redux";
import {deleteArtist, putArtistPublish} from "../../store/actions/artistActions";

const ArtistItem = (props) => {
    const user = useSelector(state => state.users.user);
    const dispatch = useDispatch();
    let cardImage = imageNotAvailable;

    if (props.image) {
        cardImage = apiURL + '/uploads/' + props.image;
    };

    const publishArtist = (id) => {
        dispatch(putArtistPublish(id));
    };

    const removeArtist = (id) => {
        dispatch(deleteArtist(id));
    };


    return (
        <div>
            <NavLink to={"/albums?artist=" + props.id}>
                <div className="card my-4" style={{"width": "18rem"}}>
                    <img src={cardImage} className="card-img-top" alt="..."/>
                    <div className="card-body">
                        <h5 className="card-title">{props.name}</h5>
                    </div>
                </div>
            </NavLink>

            {
                user && user.role === "admin" &&
                <>
                    {!props.isPublish ?
                        <button type="button" className="btn btn-secondary" onClick={()=>publishArtist(props.id)}>Publish</button> : null}
                    <button type="button" className="btn btn-danger" onClick={()=>removeArtist(props.id)}>Delete</button>
                </>
            }

        </div>

    );
};

export default ArtistItem;