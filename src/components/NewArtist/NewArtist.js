import React from "react";
import {useDispatch} from "react-redux";
import {postArtist} from "../../store/actions/artistActions";
import CreateArtist from "../../containers/CreateArtist/CreateArtist";

const NewArtist = props => {
    const dispatch = useDispatch();

    const createArtist = data => {
        console.log(data);
        dispatch(postArtist(data)).then(() => {
            props.history.push("/");
        });
    };

    return (
        <div className="container">
            <h1 className="text-dark mt-3">Add artist</h1>
            <CreateArtist onSubmit={createArtist} />
        </div>
    );
};

export default NewArtist;