import React from 'react';
import {Link} from "react-router-dom";
import {logoutUser} from "../../store/actions/userActions";
import {useDispatch, useSelector} from "react-redux";

const UserMenu = () => {
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);

    const logout = () => {
        dispatch(logoutUser());
    };

    return (
        <>
            <div>
                <Link className="nav-link" to="/track_history">Track History</Link>
                <button type="button" className="btn btn-outline-warning" onClick={logout}>Logout</button>
            </div>
            {
                user && user.role==='user' && <div className="d-block">
                <Link className="nav-link" to="/add_artist">Add artist</Link>
                <Link className="nav-link" to="/add_album">Add album</Link>
            </div>
            }
        </>
    );
};

export default UserMenu;