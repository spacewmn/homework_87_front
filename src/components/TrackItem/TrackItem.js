import React from 'react';

const TrackItem = (props) => {
    console.log(!!props.artist);
    return (
        <nav aria-label="breadcrumb" className="my-4">
            <ol className="breadcrumb">
                <li className="breadcrumb-item">{props.title}</li>
                {!!props.artist ? <li className="breadcrumb-item">{props.artist}</li> : null}
                {!!props.duration ? <li className="breadcrumb-item active" aria-current="page">{props.duration} min</li> : null}
                <li className="breadcrumb-item active" aria-current="page">{props.datetime}</li>
            </ol>
        </nav>
    );
};

export default TrackItem;