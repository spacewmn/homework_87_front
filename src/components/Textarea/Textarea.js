import React from 'react';

const Textarea = (props) => {
    return (
        <div className="input-group">
            <div className="input-group-prepend">
                <span className="input-group-text">With textarea</span>
            </div>
            <textarea className="form-control" aria-label="With textarea"
                      name={props.name}
                      onChange={props.onChange}
            />
        </div>
    );
};

export default Textarea;