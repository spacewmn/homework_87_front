import React from 'react';
import PropTypes from "prop-types";

const FormSelect = ({options, name, onChange}) => {
    let inputChildren = null;
    if (options) {
        inputChildren = options.map((option) => {
            return <option key={option._id} value={option._id}>{option.title}</option>
        })
    }
    return (
            <div className="input-group mb-3">
                <div className="input-group-prepend">
                    <label className="input-group-text" htmlFor="inputGroupSelect01">Choose </label>
                </div>
                <select className="custom-select" id="inputGroupSelect01" name={name} onChange={onChange}>
                    {inputChildren}
                </select>
            </div>
    );
};

FormSelect.propTypes = {
    error: PropTypes.string,
    options: PropTypes.arrayOf(PropTypes.object)
}

export default FormSelect;