import React from "react";
import {useDispatch} from "react-redux";
import CreateAlbum from "../../containers/CreateAlbum/CreateAlbum";
import {postAlbum} from "../../store/actions/albumActions";

const NewAlbum = props => {
    const dispatch = useDispatch();

    const createAlbum = data => {
        console.log(data);
        dispatch(postAlbum(data)).then(() => {
            props.history.push("/");
        });
    };

    return (
        <div className="container">
            <h1 className="text-dark mt-3">Add album</h1>
            <CreateAlbum onSubmit={createAlbum} />
        </div>
    );
};

export default NewAlbum;