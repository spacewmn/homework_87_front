import React from 'react';
import {apiURL} from "../../constants";
import imageNotAvailable from "../../assets/images/download.png";
import {NavLink} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {deleteAlbum, putAlbumPublish} from "../../store/actions/albumActions";

const AlbumItem = (props) => {
    const user = useSelector(state => state.users.user);
    const dispatch = useDispatch();
    let cardImage = imageNotAvailable;

    if (props.image) {
        cardImage = apiURL + '/uploads/' + props.image;
    }

    const publishAlbum = (id) => {
        dispatch(putAlbumPublish(id));
    };

    const removeAlbum = (id) => {
        dispatch(deleteAlbum(id));
    };

    return (
        <>

        <NavLink to={"/tracks?album=" + props.id}>
            <div className="media my-4 border rounded">
                <h1>{props.artist.name}</h1>
                <img src={cardImage} className="mr-3" style={{"maxWidth": "100px"}} alt="..."/>
                    <div className="media-body">
                        <h5 className="mt-0">{props.name}</h5>
                        {props.year}
                    </div>
            </div>
        </NavLink>
    {
        user && user.role === "admin" &&
        <>
            {!props.isPublish ?
                <button type="button" className="btn btn-secondary" onClick={()=>publishAlbum(props.id)}>Publish</button> : null}
            <button type="button" className="btn btn-danger" onClick={()=>removeAlbum(props.id)}>Delete</button>
        </>
    }
        </>
);
};

export default AlbumItem;