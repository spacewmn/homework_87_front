import React, {useState} from 'react';
import FormElement from "../../components/FormElement/FormElement";
import {useDispatch, useSelector} from "react-redux";
import FileInput from "../../components/FileInput/FileInput";
import Textarea from "../../components/Textarea/Textarea";

const CreateArtist = ({onSubmit}) => {
    const [state, setState] = useState({
        title: "",
        image: "",
        info: ''
    });

    // const error = useSelector(state => state.artists.loginError);
    const dispatch = useDispatch();
    // const artists = useSelector(state => state.artists);
    // const error = useSelector(state => state.error);

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });
    };

    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];
        setState(prevState => ({
            ...prevState,
            [name]: file
        }));
    };

    const formSubmitHandler = e => {
        e.preventDefault();
        let formData = new FormData();
        Object.keys(state).forEach(key => {
            formData.append(key, state[key]);
        });
        onSubmit(formData);
    };


    // const postArtist = () => {
    //     dispatch(postArtist({...state}));
    // }


    return (
        <form className="mb-5" onSubmit={formSubmitHandler}>
            <FormElement
                name='title'
                onChange={inputChangeHandler}
                label='Artist`s name'
                type='text'
                value={state.title}
            />
            <FileInput
                name="image"
                onChange={fileChangeHandler}
            />
            <Textarea
                name="info"
                onChange={inputChangeHandler}
                value={state.info}
            />
            <button type="submit" className="btn btn-primary">Add artist</button>
        </form>
    );
};

export default CreateArtist;