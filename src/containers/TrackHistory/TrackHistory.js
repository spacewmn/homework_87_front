import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import TrackItem from "../../components/TrackItem/TrackItem";
import {fetchTrackHistory} from "../../store/actions/trackHistoryAction";
import {Redirect} from "react-router-dom";

const Tracks = () => {
    const dispatch = useDispatch();
    const trackHistories = useSelector(state => state.trackHistory.trackHistory);
    const user = useSelector(state => state.users.user);

    useEffect(() => {
        dispatch(fetchTrackHistory());
    }, [dispatch]);

    if (!user) {
        return <Redirect to='/' />
    }


    return trackHistories && (
        <div className="container">
            {trackHistories.map(trackHistory => {
                return(
                    <TrackItem
                        key={trackHistory._id}
                        title={trackHistory.track.title}
                        artist={trackHistory.track.album.artist.title}
                        duration={trackHistory.duration}
                        datetime={trackHistory.datetime}
                    />
                )
            })}
        </div>
    );
};

export default Tracks;