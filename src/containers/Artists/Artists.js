import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {fetchArtists} from "../../store/actions/artistActions";
import ArtistItem from "../../components/ArtistItem/ArtistItem";

const Artists = () => {
    const dispatch = useDispatch();
    const artists = useSelector(state => state.artists.artists);

    useEffect(() => {
        dispatch(fetchArtists());
    }, [dispatch]);


    return (
    <div className="container">
        {artists.map(artist => {
            return(
                <ArtistItem
                    key={artist._id}
                    id={artist._id}
                    title={artist.title}
                    image={artist.image}
                    name={artist.title}
                    isPublish={artist.publish}
                />
            )
        })}
    </div>
    );
};

export default Artists;