import React, {useState, useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchArtists, postArtist} from "../../store/actions/artistActions";
import FormElement from "../../components/FormElement/FormElement";
import FileInput from "../../components/FileInput/FileInput";
import FormSelect from "../../components/FormSelect/FormSelect";
import {fetchAlbums} from "../../store/actions/albumActions";

const CreateAlbum = ({onSubmit}) => {
    const [state, setState] = useState({
        title: "",
        image: "",
        year: '',
        artist: ''
    });

    // const error = useSelector(state => state.artists.loginError);
    const dispatch = useDispatch();
    const artists = useSelector(state => state.artists.artists);
    // const error = useSelector(state => state.error);

    useEffect(() => {
        dispatch(fetchAlbums())
    }, [dispatch]);

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });
    };

    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];
        setState(prevState => ({
            ...prevState,
            [name]: file
        }));
    };

    const formSubmitHandler = e => {
        e.preventDefault();
        let formData = new FormData();
        console.log(state, 'state');
        Object.keys(state).forEach(key => {
            formData.append(key, state[key]);
        });
        onSubmit(formData);
    };

    return (
        <form className="mb-5" onSubmit={formSubmitHandler}>
            <FormSelect
                name='artist'
                options={artists}
                onChange={inputChangeHandler}
            />
            <FormElement
                name='title'
                onChange={inputChangeHandler}
                label='Album`s name'
                type='text'
            />
            <FileInput
                name="image"
                onChange={fileChangeHandler}
            />
            <FormElement
                name='year'
                onChange={inputChangeHandler}
                label='Year of the album'
                type='number'
            />
            <button type="submit" className="btn btn-primary">Add album</button>
        </form>
    );
};

export default CreateAlbum;