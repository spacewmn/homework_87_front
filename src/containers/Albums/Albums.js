import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import AlbumItem from "../../components/AlbumItem/AlbumItem";
import {fetchAlbums} from "../../store/actions/albumActions";

const Albums = (props) => {
    const dispatch = useDispatch();
    const albums = useSelector(state => state.albums.albums);
    useEffect(() => {
        dispatch(fetchAlbums(props.location.search));
    }, [dispatch, props.location.search]);

    return (
        <div className="container">
            {albums && albums.map(album => {
                return(
                    <AlbumItem
                        key={album._id}
                        id={album._id}
                        image={album.image}
                        name={album.title}
                        artist={album.artist}
                        year={album.year}
                    />
                )
            })}
        </div>
    );
};

export default Albums;