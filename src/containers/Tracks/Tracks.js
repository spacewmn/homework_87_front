import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import TrackItem from "../../components/TrackItem/TrackItem";
import {fetchTracks} from "../../store/actions/trackActions";

const Tracks = (props) => {
    const dispatch = useDispatch();
    const tracks = useSelector(state => state.tracks.tracks);
    useEffect(() => {
        dispatch(fetchTracks(props.location.search));
    }, [dispatch, props.location.search]);

    return (
        <div className="container">
            {tracks.map(track => {
                return(
                    <TrackItem
                        key={track._id}
                        title={track.title}
                        duration={track.duration}
                    />
                )
            })}
        </div>
    );
};

export default Tracks;