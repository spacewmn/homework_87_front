import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";
import Artists from "./containers/Artists/Artists";
import Albums from "./containers/Albums/Albums";
import Tracks from "./containers/Tracks/Tracks";
import TrackHistory from "./containers/TrackHistory/TrackHistory";
import CreateArtist from "./containers/CreateArtist/CreateArtist";
import CreateAlbum from "./containers/CreateAlbum/CreateAlbum";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import NewArtist from "./components/NewArtist/NewArtist";
import NewAlbum from "./components/NewAlbum/NewAlbum";

const ProtectedRoute = ({isAllowed, redirectTo, ...props}) => {
    return isAllowed ? <Route {...props} /> : <Redirect to={redirectTo} />
};

const Routes = ({user}) => {
    return (
        <Switch>
            <Route path="/" exact component={Artists}/>
            <Route path='/albums' exact component={Albums}/>
            <Route path='/tracks' exact component={Tracks}/>
            <ProtectedRoute
                path='/track_history'
                exact
                component={TrackHistory}
                isAllowed={user}
                redirectTo="/login"
            />
            <ProtectedRoute
                path='/add_artist'
                exact
                component={NewArtist}
                isAllowed={user}
                redirectTo="/login"
            />
            <ProtectedRoute
                path='/add_album'
                exact
                component={NewAlbum}
                isAllowed={user}
                redirectTo="/login"
            />
            <ProtectedRoute
                path='/register'
                exact
                component={Register}
                isAllowed={!user}
                redirectTo="/"
            />
            <ProtectedRoute
                path='/login'
                exact
                component={Login}
                isAllowed={!user}
                redirectTo="/"
            />
        </Switch>
    );
};

export default Routes;