import axios from "../../axiosApi";
import {
    DELETE_ALBUM_SUCCESS,
    DELETE_ARTIST_SUCCESS,
    FETCH_ALBUMS_SUCCESS, POST_ALBUM_FAILURE, POST_ALBUM_SUCCESS,
    POST_ARTIST_FAILURE,
    POST_ARTIST_SUCCESS, PUT_ALBUM_SUCCESS,
    PUT_ARTIST_SUCCESS
} from "../actionTypes";
import {push} from "connected-react-router";

const fetchAlbumsSuccess = albums => {
    return {type: FETCH_ALBUMS_SUCCESS, albums};
};

export const fetchAlbums = (link) => {
    return dispatch => {
        return axios.get("/albums" + link).then(response => {
            console.log(response.data);
            dispatch(fetchAlbumsSuccess(response.data));
        });
    };
};

const postAlbumSuccess = artist => {
    return {type: POST_ALBUM_SUCCESS, artist}
};

const postAlbumFailure = error => {
    return {type: POST_ALBUM_FAILURE, error}
};

export const postAlbum = album => {
    return async (dispatch) => {
        try {
            const response = await axios.post("/albums", album)
            dispatch(postAlbumSuccess(response.data));
            dispatch(push('/'));
        } catch (e) {
            dispatch(postAlbumFailure(e))
        }
    }
};

const putAlbumSuccess = id => {
    return {type: PUT_ALBUM_SUCCESS, id}
};

export const putAlbumPublish = id => {
    return async (dispatch) => {
        try {
            const albumPublish = {publish: true};
            await axios.put("/albums/" + id, albumPublish)
            dispatch(putAlbumSuccess(id));
        } catch (e) {
            // dispatch(postArtistFailure(e))
        }
    }
};

const deleteAlbumSuccess = id => {
    return {type: DELETE_ALBUM_SUCCESS, id}
};

export const deleteAlbum = id => {
    return async (dispatch) => {
        try {
            await axios.delete("/albums/" + id)
            dispatch(deleteAlbumSuccess(id));
        } catch (e) {
            // dispatch(postArtistFailure(e))
        }
    }
};

