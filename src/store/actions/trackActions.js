import axios from "../../axiosApi";
import {FETCH_TRACKS_SUCCESS} from "../actionTypes";

const fetchTracksSuccess = tracks => {
    return {type: FETCH_TRACKS_SUCCESS, tracks};
};

export const fetchTracks = (link) => {
    return dispatch => {
        return axios.get("/tracks" + link).then(response => {
            console.log(response.data);
            dispatch(fetchTracksSuccess(response.data));
        });
    };
};