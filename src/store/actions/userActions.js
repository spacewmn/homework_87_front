import {
    LOGIN_USERS_FAILURE,
    LOGIN_USERS_SUCCESS,
    LOGOUT_USER,
    REGISTER_USERS_FAILURE,
    REGISTER_USERS_SUCCESS
} from "../actionTypes";
import axios from '../../axiosApi';
import {push} from 'connected-react-router';

const registerUserSuccess = () => {
    return {type: REGISTER_USERS_SUCCESS}
};

const registerUserFailure = error => {
    return{type: REGISTER_USERS_FAILURE, error}
};

export const registerUser = userData => {
    return async dispatch => {
        try {
            await axios.post('/users', userData);
            dispatch(registerUserSuccess());
            dispatch(push('/'))
        } catch (e) {
            dispatch(registerUserFailure(e));
            if (e.response && e.response.data) {
                dispatch(registerUserFailure(e.response.data));
            } else {
                dispatch(registerUserFailure({global: 'No Internet'}));
            }
        }
    }
};

const loginUserSuccess = user => {
    return {type: LOGIN_USERS_SUCCESS, user}
};

const loginUserFailure = error => {
    return {type: LOGIN_USERS_FAILURE, error}
};

export const loginUser = userData => {
    return async dispatch => {
        try {
            const response = await axios.post('/users/sessions', userData);
            dispatch(loginUserSuccess(response.data));
            dispatch(push('/'));
        } catch (e) {
            dispatch(loginUserFailure(e))
        }
    }
};

export const logoutUser = () => {
    return async (dispatch, getState) => {
        const token = getState().users.user.token;
        const headers = {"Authorization": token};
        await axios.delete('/users/sessions', {headers});
        dispatch({type: LOGOUT_USER});
        dispatch(push('/'));
    }
}

