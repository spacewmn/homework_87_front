import axios from "../../axiosApi";
import {FETCH_TRACK_HISTORY_SUCCESS} from "../actionTypes";

const fetchTrackHistorySuccess = tracks => {
    return {type: FETCH_TRACK_HISTORY_SUCCESS, tracks};
};

export const fetchTrackHistory = () => {
    return async (dispatch, getState) => {
        const headers = {
            "Authorization": getState().users.user && getState().users.user.token
        }
        const response = await axios.get("/track_history", {headers})
        dispatch(fetchTrackHistorySuccess(response.data));
    };
};