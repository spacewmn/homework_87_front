import axios from "../../axiosApi";
import {
    DELETE_ARTIST_SUCCESS,
    FETCH_ARTISTS_SUCCESS,
    POST_ARTIST_FAILURE,
    POST_ARTIST_SUCCESS,
    PUT_ARTIST_SUCCESS
} from "../actionTypes";
import {push} from "connected-react-router";

const fetchArtistsSuccess = artists => {
    return {type: FETCH_ARTISTS_SUCCESS, artists};
};

export const fetchArtists = () => {
    return async (dispatch, getState) => {
        const token = getState().users.user && getState().users.user.token;
        const headers = {"Authorization": token};
        const response = await axios.get("/artists", {headers})
        // console.log(response);
        dispatch(fetchArtistsSuccess(response.data));

    };
};

const postArtistSuccess = artist => {
    return {type: POST_ARTIST_SUCCESS, artist}
};

const postArtistFailure = error => {
    return {type: POST_ARTIST_FAILURE, error}
};

export const postArtist = artist => {
    return async (dispatch) => {
        try {
            const response = await axios.post("/artists", artist)
            dispatch(postArtistSuccess(response.data));
            dispatch(push('/'));
        } catch (e) {
            dispatch(postArtistFailure(e))
        }
    }
};

const putArtistSuccess = id => {
    return {type: PUT_ARTIST_SUCCESS, id}
};

export const putArtistPublish = id => {
    return async (dispatch) => {
        try {
            const artistPublish = {publish: true};
            await axios.put("/artists/" + id, artistPublish)
            dispatch(putArtistSuccess(id));
            // dispatch(push('/'));
        } catch (e) {
            // dispatch(postArtistFailure(e))
        }
    }
};

const deleteArtistSuccess = id => {
    return {type: DELETE_ARTIST_SUCCESS, id}
};

export const deleteArtist = id => {
    return async (dispatch) => {
        try {
            // const artistPublish = {publish: true};
            await axios.delete("/artists/" + id)
            dispatch(deleteArtistSuccess(id));
            // dispatch(push('/'));
        } catch (e) {
            // dispatch(postArtistFailure(e))
        }
    }
};

