import {DELETE_ALBUM_SUCCESS, FETCH_ALBUMS_SUCCESS, PUT_ALBUM_SUCCESS} from "../actionTypes";

const initialState = {
    albums: []
};

const albumReducer = (state = initialState, action) => {
    switch(action.type) {
        case FETCH_ALBUMS_SUCCESS:
            return {...state, albums: action.albums};
        case PUT_ALBUM_SUCCESS:
            const index = state.albums.findIndex((element) => {
                return element._id === action.id
            })
            const copyState = {...state}
            const copyAlbums = [...copyState.albums];
            const album = copyAlbums[index];
            album.publish = true;
            copyAlbums[index] = album;
            return {...state, albums: [...copyAlbums]};
        case DELETE_ALBUM_SUCCESS:
            const ind = state.albums.findIndex((element) => {
                return element._id === action.id
            })
            const copyStateAlb = {...state}
            const copyAlbumArr = [...copyStateAlb.albums];
            copyAlbumArr.splice(ind, 1)
            return {...state, albums: [...copyAlbumArr]}
        default:
            return state;
    }
};

export default albumReducer;