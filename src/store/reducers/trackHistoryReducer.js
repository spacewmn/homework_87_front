import {FETCH_TRACK_HISTORY_SUCCESS} from "../actionTypes";

const initialState = {
    trackHistory: null
};

const trackHistoryReducer = (state = initialState, action) => {
    switch(action.type) {
        case FETCH_TRACK_HISTORY_SUCCESS:
            return {...state, trackHistory: action.tracks};
        default:
            return state;
    }
};

export default trackHistoryReducer;