import {DELETE_ARTIST_SUCCESS, FETCH_ARTISTS_SUCCESS, PUT_ARTIST_SUCCESS} from "../actionTypes";

const initialState = {
    artists: []
};

const artistReducer = (state = initialState, action) => {
    switch(action.type) {
        case FETCH_ARTISTS_SUCCESS:
            return {...state, artists: action.artists};
        case PUT_ARTIST_SUCCESS:
            const index = state.artists.findIndex((element) => {
                return element._id === action.id
            })
            const copyState = {...state}
            const copyArtists = [...copyState.artists];
            const artist = copyArtists[index];
            artist.publish = true;
            copyArtists[index] = artist;
            return {...state, artists: [...copyArtists]};
        case DELETE_ARTIST_SUCCESS:
            const ind = state.artists.findIndex((element) => {
                return element._id === action.id
            })
            const copyStateArt = {...state}
            const copyArtistsArr = [...copyStateArt.artists];
            copyArtistsArr.splice(ind, 1)
            return {...state, artists: [...copyArtistsArr]}

        default:
            return state;
    }
};

export default artistReducer;