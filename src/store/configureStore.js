import React from 'react';
import {applyMiddleware, combineReducers, compose, createStore} from 'redux';
import thunkMiddleware from 'redux-thunk';
import {createBrowserHistory} from "history";
import {connectRouter, routerMiddleware} from "connected-react-router";
import artistReducer from '../store/reducers/artistReducer';
import albumReducer from "../store/reducers/albumReducer";
import trackReducer from "../store/reducers/trackReducer";
import 'bootstrap/dist/css/bootstrap.min.css';
import usersReducer from "../store/reducers/userReducer";
import trackHistoryReducer from "../store/reducers/trackHistoryReducer";
import {loadFromLocalStorage, saveToLocalStorage} from "./localStore";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const history = createBrowserHistory();
const rootReducer = combineReducers({
    artists: artistReducer,
    albums: albumReducer,
    tracks: trackReducer,
    users: usersReducer,
    trackHistory: trackHistoryReducer,
    router: connectRouter(history)
});

const middleware = [
    thunkMiddleware,
    routerMiddleware(history)
];


const enhancers = composeEnhancers(applyMiddleware(...middleware));

const persistedState = loadFromLocalStorage();

const store = createStore(rootReducer, persistedState, enhancers);

store.subscribe(() => {
    saveToLocalStorage({
        users: {
            user: store.getState().users.user
        }
    });
});

export default store;